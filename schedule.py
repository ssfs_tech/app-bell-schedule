from ics import Calendar
import requests
import arrow
import csv


url = 'https://calendar.google.com/calendar/ical/ssfs.org_1u88ibj7pv3ilm9dk56t3nhp5o%40group.calendar.google.com/public/basic.ics'
c = Calendar(requests.get(url).text)
with open("output.csv", "w", newline="") as test_file:
    my_file = csv.writer(test_file, 'excel')
    for event in sorted(c.events):
        start = event.begin
        end = event.end
        if event.all_day:
            my_file.writerow([event.name, start.format("M/D/YYYY"), "12:00 AM", end.format("M/D/YYYY"),"12:00 AM"])
        else:
            start = start.to('US/Eastern')
            end = end.to('US/Eastern')
            my_file.writerow([event.name, start.format("M/D/YYYY"), start.format("h:mm A"), end.format("M/D/YYYY"),
                           end.format("h:mm A")])
        # my_file.write(str(event.name) + "," + start.format("M/D/YYYY") + "," + start.format("h:mm") + "," +
        #                 end.format("M/D/YYYY") + "," + end.format("h:mm") + "\n")



